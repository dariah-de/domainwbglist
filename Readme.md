# Domain List sorter

## Setup

Install python dependencies on Ubuntu:

```bash
apt-get install python3-pip python3-virtualenv libapache2-mod-wsgi-py3 python3-dev build-essential libffi-dev libldap2-dev libsasl2-dev libssl-dev
```

Start by creating a virtualenv

``` bash
virtualenv venv
source venv/bin/activate
```

than install the dependencies

``` bash
pip install -r requirements.txt
```

## Config

Install flask with its components from `reqirements.txt` into a virtualenv namend `venv`.

Enforce Shibboleth authentication at `/login`.

Make sure the server can write to `grey.txt`.

The following options must be set in `settings_local.py`:

```python
SECRET_KEY = '*************'

LDAP_URL = 'ldap://mu.ldap.vm'
LDAP_USER = 'uid=user,dc=example,dc=com'
LDAP_PASS = '**************'

LDAP_WHITE_DN = 'dc=whitelist,dc=example,dc=com'
LDAP_BLACK_DN = 'dc=blacklist,dc=example,dc=com'

LDAP_MAIL_SEARCH_BASE = 'dc=example,dc=com'

ADMIN_GROUPS = 'admin'
```

If needed, you can overwrite `SSO_ATTRIBUTE_MAP`.

To enable debugging add

```python
DEBUG = True
```

If you want to enable the use of eduGAIN's isFederatedCheck, add

```python
EDUGAIN_CHECK = True
```

## Nutzung der DARIAH white/grey/black list

URL: <https://ci.de.dariah.eu/domainwbglist/>

### Nutzung ohne Login

Ohne Login ist eine Prüfung möglich, ob eine Domain auf der white list, grey list oder black list eingetragen oder unbekannt ist. Hierfür wird eine Mailadresse in die Sucheleiste eingetragen und auf “Prüfen” geklickt. Ein Pop-Up informiert über den Status.

### Nutzung mit Login

Oben genannte Prüfung ist auch mit Login möglich.

Sollte die Mailadresse unbekannt sein, erscheint nun im Pop-Up der Button “Decide”. Hier kann zuerst ein eduGain-Test durchgeführt werden, dann ist eine Einordnung in die drei bekannten Listen möglich.
Bei bekannter Mailadresse ist es möglich, die Listeneinordnung zu ändern. Der beschrittene Weg ist danach derselbe.

Die drei Listen befinden sich im unteren Bereich der Seite. Bei Klick auf einzelne Einträge können auch hier die Einordnungen verändert werden.

### Sinn der Einordnung

Bei DARIAH-Accountregistrierungen über das Formular wird die wbg-Liste abgefragt. Befindet sich die Domain auf der white list, wird der DARIAH-Account sofort automatisch angelegt; eine manuelle Freigabe des Account ist nicht mehr nötig. Auf der black list geführte Domains erhalten keinen Account.
